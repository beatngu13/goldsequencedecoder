package org.bitbucket.beatngu13.goldsequencedecoder;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
	
	public static void main(String[] args) {
		String pathname = args[0];
		
		if (pathname != null) {
			File file = new File(pathname);
			
			if (file.exists()) {
				String sequenceString = null;
				
				try (BufferedReader br = new BufferedReader(
						new InputStreamReader(new FileInputStream(file)))) {
					sequenceString = br.readLine();
				} catch (IOException e) {
					e.printStackTrace();
				}
				
				if (sequenceString != null) {
					String[] splittedSequenceString = sequenceString.split(" ");
					int sequenceLength = 
							GoldSequenceDecoder.SEQUENCE_LENGTH;
					int[] parsedSequence = new int[sequenceLength];
					
					for (int i = 0; i < sequenceLength; i++) {
						parsedSequence[i] = 
								Integer.parseInt(splittedSequenceString[i]);
					}
					
					GoldSequenceDecoder decoder = new GoldSequenceDecoder();
					int[][] result = decoder.decode(parsedSequence);
					
					for (int i = 0; i < result.length; i++) {
						int id = result[i][0];
						String separator = id < 10 ? "  " : " ";
						
						System.out.println("Satellite" + separator + id 
								+ " has sent bit " + result[i][1] 
								+ " (delta = " + result[i][2] + ")");
					}
				}		
			}
		}
	}
	
}
