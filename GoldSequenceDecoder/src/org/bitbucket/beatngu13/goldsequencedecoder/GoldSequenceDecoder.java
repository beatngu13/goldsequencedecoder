/**
 * 
 */
package org.bitbucket.beatngu13.goldsequencedecoder;

import java.util.Arrays;

/**
 * A Gold sequence decoder, which decrypts a given GPS sequence.
 * 
 * @author danielkraus1986@gmail.com
 *
 */
public class GoldSequenceDecoder {
	
	/**
	 * The number of sending satellites.
	 */
	public static final int NUMBER_OF_SENDERS = 4;
	/**
	 * The length of the shift registers.
	 */
	public static final int REGISTER_LENGTH = 10;
	/**
	 * The length of the GPS sequence.
	 */
	public static final int SEQUENCE_LENGTH = 1023;

	/**
	 * The higher limit to detect preaks.
	 */
	private final int upperLimit = SEQUENCE_LENGTH - 3 
			* (int) (Math.pow(2, (REGISTER_LENGTH + 2) / 2) - 1);
	/**
	 * The lower limit to detect peaks.
	 */
	private final int lowerLimit = -upperLimit;
	/**
	 * The two operators for each satellite's bottom shift register.
	 */
	private final int[][] operators = {
		{2, 6}, {3, 7}, {4, 8}, {5, 9}, 
		{1, 9}, {2, 10}, {1, 8}, {2, 9}, 
		{3, 10}, {2, 3}, {3, 4}, {5, 6}, 
		{6, 7}, {7, 8}, {8, 9}, {9, 10}, 
		{1, 4}, {2, 5}, {3, 6}, {4, 7}, 
		{5, 8}, {6, 9}, {1, 3}, {4, 6}
		};
	
	/**
	 * The GPS satellites.
	 */
	private Satellite[] satellites = 
			new Satellite[Satellite.NUMBER_OF_SATELLITES];
	
	public GoldSequenceDecoder() {
		for (int i = 0; i < Satellite.NUMBER_OF_SATELLITES; i++) {
			satellites[i] = 
					new Satellite(i + 1, operators[i][0], operators[i][1]);
		}
	}
	
	/**
	 * Decodes the given GPS sequence.
	 * 
	 * @param gpsSequence The GPS sequence input.
	 * @return A result set for each found satellite, containing the ID 
	 * (<code>result[i][0]</code>), the bit which was sent 
	 * (<code>result[i][1]</code>) and the delta (<code>result[i][2]</code>).
	 */
	public int[][] decode(int[] gpsSequence) {
		int[] doubledGpsSequence = new int[2 * SEQUENCE_LENGTH];
		int[][] resultSet = new int[NUMBER_OF_SENDERS][3];
		int found = 0;
		
		// Avoid shifting by using a sliding window for cross-correlation.
		for (int i = 0; i < gpsSequence.length; i++) {
			doubledGpsSequence[i] = gpsSequence[i];
			doubledGpsSequence[i + SEQUENCE_LENGTH] = gpsSequence[i];
		}
		
		for (Satellite s : satellites) {
			int[] currentChipSequence = s.getChipSequence();
			
			int bit = 0b0;
			int delta = 0;
			boolean isSender = false;
			
			for (int i = 0; i < SEQUENCE_LENGTH; i++) {
				int[] slidingWindow = Arrays.copyOfRange(doubledGpsSequence, i, 
						SEQUENCE_LENGTH + i + 1);
				int scalar = calculateScalarProduct(currentChipSequence,
						slidingWindow);
				boolean belowLowerLimit = scalar < lowerLimit;
				boolean aboveUpperLimit = scalar > upperLimit;
				
				if (belowLowerLimit || aboveUpperLimit) {
					bit = belowLowerLimit ? 0b0 : 0b1;
					delta = i;
					isSender = true;
					
					break;
				}
			}
			
			if (isSender) {
				resultSet[found][0] = s.getId();
				resultSet[found][1] = bit;
				resultSet[found][2] = delta;
				
				if (++found == NUMBER_OF_SENDERS) {
					break;
				}
			}
		}
		
		return resultSet;
	}
	
	/**
	 * Calculates the scalar product for two given vectors.
	 * 
	 * @param v1 First vector.
	 * @param v2 Second vector.
	 * @return The scalar product.
	 */
	private int calculateScalarProduct(int[] v1, int[] v2) {
		int scalarProduct = 0;
		
		for (int i = 0; i < v1.length; i++) {
			scalarProduct += v1[i] * v2[i];
		}
		
		return scalarProduct;
	}
	
}
