/**
 * 
 */
package org.bitbucket.beatngu13.goldsequencedecoder;

import java.util.Arrays;

/**
 * GPS satellite.
 * 
 * @author danielkraus1986@gmail.com
 *
 */
public class Satellite {
	
	/**
	 * Total number of GPS satellites.
	 */
	public static final int NUMBER_OF_SATELLITES = 24;
	
	/**
	 * The initial state of each shift register.
	 */
	private final int[] initialRegister = 
			new int[]{0b1, 0b1, 0b1, 0b1, 0b1, 0b1, 0b1, 0b1, 0b1, 0b1};
	/**
	 * The index of the top operator. XOR together with the last register value, 
	 * produces the new first value for the top shift register.
	 */
	private final int topOperator = 2;
	/**
	 * The indices of the bottom operators. XOR together with the last register 
	 * value, produces the new first value for the bottom register.
	 */
	private final int[] bottomOperators = new int[]{1, 2, 5, 7, 8};
	
	/**
	 * The satellite ID.
	 */
	private int id;
	/**
	 * The first operator to XOR the final bottom value.
	 */
	private int operator1;
	/**
	 * The second operator to XOR the final bottom value.
	 */
	private int operator2;
	
	/**
	 * The satellite's chip sequence.
	 */
	private int[] chipSequence = 
			new int[GoldSequenceDecoder.SEQUENCE_LENGTH];
	
	/**
	 * @param id {@link #id}.
	 * @param operator1 {@link #operator1}.
	 * @param operator2 {@link #operator2}.
	 */
	public Satellite(int id, int operator1, int operator2) {
		this.id = id;
		this.operator1 = operator1;
		this.operator2 = operator2;
		
		computeChipSequence();
	}
	
	/**
	 * Computes the chip sequence.
	 */
	private final void computeChipSequence() {
		int registerLength = GoldSequenceDecoder.REGISTER_LENGTH;
		int[] topRegister = Arrays.copyOf(initialRegister, registerLength);
		int[] bottomRegister = Arrays.copyOf(initialRegister, registerLength);
		int indexOp1 = operator1 - 1;
		int indexOp2 = operator2 - 1;
		
		for (int i = 0; i < GoldSequenceDecoder.SEQUENCE_LENGTH; i++) {
			// Operators of the final XOR, which produces the chip sequence.
			int topOp = topRegister[registerLength - 1];
			int bottomOp = bottomRegister[indexOp1] ^ bottomRegister[indexOp2];
			chipSequence[i] = (topOp ^ bottomOp) == 0b1 ? 1 : -1;
			
			// Evaluation of both next first values.
			int nextTopFirst = topOp ^ topRegister[topOperator];
			int nextBottomFirst = bottomRegister[registerLength - 1];
			
			for (int j = 0; j < bottomOperators.length; j++) {
				nextBottomFirst ^= bottomRegister[bottomOperators[j]];
			}
			
			// Shift both registers, except the first values.
			for (int j = registerLength - 1; j > 0; j--) {
				topRegister[j] = topRegister[j - 1];
				bottomRegister[j] = bottomRegister[j - 1];
			}
			
			topRegister[0] = nextTopFirst;
			bottomRegister[0] = nextBottomFirst;
		}
	}

	/**
	 * @return {@link #id}.
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return {@link #chipSequence}.
	 */
	public int[] getChipSequence() {
		return chipSequence;
	}
	
}
